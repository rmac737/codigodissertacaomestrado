package websocketserver;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import javax.imageio.ImageIO;
import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import org.glassfish.tyrus.server.Server;


/**
 *
 * @author roberto.macedo
 */
@ServerEndpoint(value = "/")
public class WebSocketServer {

    static boolean continua = true;
    static boolean isBusy = false;
    static Session sessao;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here        
        Server server = new Server("localhost", 81, "/", null, WebSocketServer.class);
        try {
            server.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            while(continua)
            {
                //espera ocupada para não derrubar o servidor websocket
            }
        } catch (Exception e) {
            continua = false;
            e.printStackTrace();
        } finally {
            //server.stop(); //por enquanto não será stopped
        }
    }
    
    @OnMessage
    public void onMessage(String message, Session session) 
    {
        try
        {
            sessao = session;
            isBusy = true;
            System.out.println("Mensagem recebida pelo NodeMCU: " + message);
            sessao.getBasicRemote().sendText("Mensagem enviada pelo servidor WebSocket");
        }
        catch (Exception ex)
        {
          System.out.println("Exception: " + ex);    
        }
    }
    
    public void sendMessageBackToConnectedClient(String msg)
    {        
        try
        {             
            sessao.getBasicRemote().sendText(msg);
        }
        catch(Exception ex)
        {
            
        }
    }
}
