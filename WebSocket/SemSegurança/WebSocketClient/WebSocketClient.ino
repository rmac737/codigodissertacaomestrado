
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WebSocketsClient.h>
#include <Hash.h>

ESP8266WiFiMulti WiFiMulti;
WebSocketsClient webSocket;
long startTime; 
long elapsedTime;
long conectado=0;

#define USE_SERIAL Serial

void webSocketEvent(WStype_t type, uint8_t * payload, size_t length) {

  switch(type) {
    case WStype_DISCONNECTED:
      conectado = 0;
      break;
    case WStype_CONNECTED: {
      USE_SERIAL.printf("[WSc] Connected to url: %s\n", payload);
      conectado = 1;
      
    }
      break;
    case WStype_TEXT:
      //USE_SERIAL.printf("[WSc] get text: %s\n", payload);
      break;
    case WStype_BIN:
      //USE_SERIAL.printf("[WSc] get binary length: %u\n", length);
      //hexdump(payload, length);

      // send data to server
      // webSocket.sendBIN(payload, length);
      break;
  }
}

void setup() {
  // USE_SERIAL.begin(921600);
  USE_SERIAL.begin(115200);

  //Serial.setDebugOutput(true);
  USE_SERIAL.setDebugOutput(true);

  USE_SERIAL.println();
  USE_SERIAL.println();
  USE_SERIAL.println();

  
  for(uint8_t t = 4; t > 0; t--) {
    USE_SERIAL.printf("[SETUP] BOOT WAIT %d...\n", t);
    USE_SERIAL.flush();
    delay(200);
  }  

  WiFiMulti.addAP("AndroidAP", "12345678");

  //WiFi.disconnect();
  while(WiFiMulti.run() != WL_CONNECTED) {
    delay(100);
  } 
 
  // server address, port and URL
  webSocket.begin("192.168.43.92", 81, "/chat");       

  // event handler
  webSocket.onEvent(webSocketEvent);

  // use HTTP Basic Authorization this is optional remove if not needed
  //webSocket.setAuthorization("user", "Password");

  // try ever 5000 again if connection has failed
  webSocket.setReconnectInterval(5000); 
} 

void loop() {
  webSocket.loop();  
  if(conectado == 1)//conectado
  { 
    //Coleta do Teste      
      for(int i=0;i<10000;i++) 
      { 
          Serial.print(i+1);
          Serial.print(" - ");
          startTime = millis(); //Início da contagem do tempo
          //1500bytes//webSocket.sendTXT("[{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'}]");
          //2500bytes//webSocket.sendTXT("[{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'}]");
          //3500bytes
          webSocket.sendTXT("[{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'}]");
          
          elapsedTime =   millis() - startTime; //Fim da contagem do tempo
          Serial.print("Round Trip Time(ms): ");
          Serial.print(elapsedTime);
          Serial.println(""); 
          delay(1000);
      }
  }
  else //sem conexão
  {    
      Serial.println("Disconnected");  
  }
  delay(10000);
}
