
#include <ESP8266WiFi.h>.
#include <ESP8266HTTPClient.h>

const char* ssid = "projetodissertacao";
const char* password = "projetodissertacao";

byte buffer[32];
int bufOffset = 0;
byte startByte = 0;
byte nextByte = 0;
long startTime ; 
long elapsedTime ;
int contador = 1;
 
void setup () {
    
    Serial.begin(115200);
    WiFi.begin(ssid, password);
   
    while (WiFi.status() != WL_CONNECTED) 
    {
      delay(3000);
      Serial.println("Conectando...");
    }  
  
    Serial.println("Conectado");
}

void loop()
{
  if(contador <= 200) //numero de repetições
  {
    if (WiFi.status() == WL_CONNECTED)   
    {  
        startTime = millis(); //Início da contagem do tempo

        Serial.print(contador);
        Serial.print(" - ");
        //Check WiFi connection status
        HTTPClient http;  //Declare an object of class HTTPClient

        http.begin("http://192.168.1.6/ProjetoDissertacao/ProjetoDissertacao/Post/");
        http.addHeader("Content-Type", "text/plain");   

        String var500bytes = "[{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.02010201020121515105051515'}]";
        String var1000bytes = "[{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.02010201020'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201,-20.02'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.020,-20.020'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201'}]";
        String var1500bytes = "[{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.02010201020120505055050505055'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.02010201020120550505051550505050,-20.020102010201202020202'}]";
        String msg = var1500bytes;
          
        int httpCode = http.POST(msg);
      
        if (httpCode > 0) { //Checa o código de retorno     
          //String payload = http.getString();   //Obtém o payload da resposta da requisição
        }else
        {
          Serial.print("Código do Erro:");  
          String payload = http.getString();
          Serial.println(httpCode); 
        }

        elapsedTime =   millis() - startTime; //Fim da contagem do tempo
        Serial.print("Round Trip Time(ms): ");
        Serial.print(elapsedTime);
        Serial.println("");

        http.end();
    }
    contador = contador + 1;
  }
}
