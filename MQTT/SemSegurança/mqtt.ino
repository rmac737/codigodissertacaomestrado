#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"

/************************* WiFi Access Point *********************************/

#define WLAN_SSID       "projetodissertacao"
#define WLAN_PASS       "projetodissertacao"

/************************* Adafruit.io Setup *********************************/

#define AIO_SERVER      "192.168.1.7"
#define AIO_SERVERPORT  1883                   // use 8883 for SSL
#define AIO_USERNAME    "uerj"
#define AIO_KEY         "uerj"

long startTime; 
long elapsedTime;

// Create an ESP8266 WiFiClient class to connect to the MQTT server.
WiFiClient client;
// or... use WiFiFlientSecure for SSL
//WiFiClientSecure client;

// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);

/****************************** Feeds ***************************************/

// Setup a feed called 'photocell' for publishing.
// Notice MQTT paths for AIO follow the form: <username>/feeds/<feedname>
Adafruit_MQTT_Publish pub = Adafruit_MQTT_Publish(&mqtt, "/topic/test",1);

// Setup a feed called 'onoff' for subscribing to changes.
Adafruit_MQTT_Subscribe sub = Adafruit_MQTT_Subscribe(&mqtt, "/topic/test",1);

void MQTT_connect();

void setup() {
  Serial.begin(115200);
  delay(10);
  
  // Connect to WiFi access point.
  Serial.println(); Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);

  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.println("WiFi connected");
  Serial.println("IP address: "); Serial.println(WiFi.localIP());
  // Setup MQTT subscription.
  //mqtt.subscribe(&onoffbutton);
}

uint32_t x=0;

void loop() {
  // Ensure the connection to the MQTT server is alive (this will make the first
  // connection and automatically reconnect when disconnected).  See the MQTT_connect
  // function definition further below.
  MQTT_connect();

  // this is our 'wait for incoming subscription packets' busy subloop
  // try to spend your time here

  /*
  Adafruit_MQTT_Subscribe *subscription;
  while ((subscription = mqtt.readSubscription(5000))) {
    if (subscription == &onoffbutton) {
      Serial.print(F("Got: "));
      Serial.println((char *)onoffbutton.lastread);
    }
  } 
  */

  for(int i=0;i<1000;i++)
  {
        String var500bytes = "[{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.020102010201215'}]";
        String var1000bytes = "[{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.02010201020'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201,-20.02'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.020,-20.020'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,'}]";
        String var1500bytes = "[{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.0201020102012'},{'Id':1,'InfoSensor':'Temp:10C,Vel:90kmh,Alt:100m,CoordGPS:-10.0201020102012,-20.02010201020120505055050505055'},{'Id':1,'InfoSensor':'Temp:10C,{'Id':1,'InfoSensor']";
        String msg = var1500bytes;
        
        startTime = millis(); //Início da contagem do tempo
        Serial.print(i);
        Serial.print(" - ");
    
        char __mensagem[msg.length()];
        msg.toCharArray(__mensagem, msg.length());

        //Serial.print("Tamanho: ");
        //Serial.println(msg.length());
        if (!pub.publish(__mensagem)) {
          Serial.println(F("Failed"));
        } 
        else {
          //Serial.println(F("OK!"));
        }

        elapsedTime =   millis() - startTime; //Fim da contagem do tempo
        Serial.print("Round Trip Time(ms): ");
        Serial.print(elapsedTime);
        Serial.println("");
        //delay(1000);
  }
  delay(20000);
}

 void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}
